#!/usr/bin/env bash
#
# Install yum-utils
#

set -o errexit

yum install -y yum-utils
