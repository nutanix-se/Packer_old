#!/usr/bin/env bash
#
# Install net-tools
#

set -o errexit

yum install -y net-tools
