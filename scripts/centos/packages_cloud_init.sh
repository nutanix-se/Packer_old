#!/usr/bin/env bash
#
# Install cloud-init
#

set -o errexit

yum install -y cloud-init
