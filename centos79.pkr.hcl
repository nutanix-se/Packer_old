packer {
  required_plugins {
    qemu = {
      version = ">= 1.0.1"
      source  = "github.com/hashicorp/qemu"
    }
  }
}

source "qemu" "centos79" {
  iso_url            = "http://centos-distro.cavecreek.net/7.9.2009/isos/x86_64/CentOS-7-x86_64-Minimal-2009.iso"
  iso_checksum       = "file:http://centos-distro.cavecreek.net/7.9.2009/isos/x86_64/sha256sum.txt"
  output_directory   = "output"
  shutdown_command   = "sudo -S shutdown -P now"
  disk_size          = "50G"
  format             = "qcow2"
  accelerator        = "kvm"
  http_directory     = "http"
  ssh_username       = "root"
  ssh_password       = "nutanix/4u"
  ssh_timeout        = "60m"
  vm_name            = "centos-7.9-x86_64.qcow2"
  net_device         = "virtio-net"
  disk_interface     = "virtio"
  boot_wait          = "10s"
  boot_command       = ["<tab> text ks=http://{{ .HTTPIP }}:{{ .HTTPPort }}/centos-7.9-ahv-x86_64.cfg<enter><wait>"]
  headless           = true
  disk_detect_zeroes = "unmap"
  skip_compaction    = false
  disk_compression   = true
  vnc_bind_address   = "0.0.0.0"
}

source "qemu" "centos79-lvm" {
  iso_url            = "http://centos-distro.cavecreek.net/7.9.2009/isos/x86_64/CentOS-7-x86_64-Minimal-2009.iso"
  iso_checksum       = "file:http://centos-distro.cavecreek.net/7.9.2009/isos/x86_64/sha256sum.txt"
  output_directory   = "output"
  shutdown_command   = "sudo -S shutdown -P now"
  disk_size          = "50G"
  format             = "qcow2"
  accelerator        = "kvm"
  http_directory     = "http"
  ssh_username       = "root"
  ssh_password       = "nutanix/4u"
  ssh_timeout        = "60m"
  vm_name            = "centos-7.9-x86_64-lvm.qcow2"
  net_device         = "virtio-net"
  disk_interface     = "virtio"
  boot_wait          = "10s"
  boot_command       = ["<tab> text ks=http://{{ .HTTPIP }}:{{ .HTTPPort }}/centos-7.9-ahv-x86_64-lvm.cfg<enter><wait>"]
  headless           = true
  disk_detect_zeroes = "unmap"
  skip_compaction    = false
  disk_compression   = true
  vnc_bind_address   = "0.0.0.0"
}

source "qemu" "centos79-uefi" {
  iso_url            = "http://centos-distro.cavecreek.net/7.9.2009/isos/x86_64/CentOS-7-x86_64-Minimal-2009.iso"
  iso_checksum       = "file:http://centos-distro.cavecreek.net/7.9.2009/isos/x86_64/sha256sum.txt"
  output_directory   = "output"
  shutdown_command   = "sudo -S shutdown -P now"
  disk_size          = "50G"
  format             = "qcow2"
  accelerator        = "kvm"
  http_directory     = "http"
  ssh_username       = "root"
  ssh_password       = "nutanix/4u"
  ssh_timeout        = "30m"
  vm_name            = "centos-7.9-x86_64-uefi.qcow2"
  net_device         = "virtio-net"
  disk_interface     = "virtio"
  boot_wait          = "10s"
  boot_command       = ["<tab> text ks=http://{{ .HTTPIP }}:{{ .HTTPPort }}/centos-7.9-ahv-x86_64-uefi.cfg<enter><wait>"]
  headless           = true
  disk_detect_zeroes = "unmap"
  skip_compaction    = false
  disk_compression   = true
  firmware           = "OVMF.fd"
  vnc_bind_address   = "0.0.0.0"
}

build {
  sources = ["source.qemu.centos79"]
  provisioner "shell" {
    #execute_command = "sudo -E bash '{{ .Path }}'"
    scripts = [
      "scripts/centos/security_updates.sh",
      "scripts/centos/ntnx_kernel_settings.sh",
      "scripts/centos/ntnx_set_max_sectors_kb.sh",
      "scripts/centos/ntnx_set_disk_timeout.sh",
      "scripts/centos/ntnx_set_noop.sh",
      "scripts/centos/ntnx_disable_transparent_hugepage.sh",
      "scripts/centos/ntnx_iscsi_settings.sh",
      "scripts/centos/ntnx_grub2_mkconfig.sh",
      "scripts/centos/packages_yum_tools.sh",
      "scripts/centos/packages_cloud_init.sh",
      "scripts/centos/packages_net_tools.sh",
      "scripts/linux-common/get_cloud-init_config.sh",
      "scripts/linux-sysprep/sysprep-op-dhcp-client-state.sh",
      "scripts/linux-sysprep/sysprep-op-firewall-rules.sh",
      "scripts/linux-sysprep/sysprep-op-machine-id.sh",
      "scripts/linux-sysprep/sysprep-op-mail-spool.sh",
      "scripts/linux-common/cleanup-network.sh",
      "scripts/linux-sysprep/sysprep-op-package-manager-cache.sh",
      "scripts/linux-common/cleanup-rpm-db.sh",
      "scripts/linux-sysprep/sysprep-op-ssh-hostkeys.sh",
      "scripts/linux-sysprep/sysprep-op-tmp-files.sh",
      "scripts/linux-sysprep/sysprep-op-yum-uuid.sh",
      "scripts/linux-common/cleanup-disk-space.sh",
      "scripts/linux-sysprep/sysprep-op-crash-data.sh",
      "scripts/linux-sysprep/sysprep-op-logfiles.sh",
      "scripts/linux-sysprep/sysprep-op-bash-history.sh",
    ]
  }
}
